from pg.interfaces.iobserver import IObserver


class Observer(IObserver):
    def __init__(self):
        self._processed_count = 0
        self.is_finished = False
        self._posts_count = 1

    def inc(self):
        self._processed_count += 1

    def inc(self, count):
        self._processed_count += count

    def set(self, count):
        self._processed_count = count

    def finish(self):
        self.is_finished = True

    def set_posts_count(self, count):
        self._posts_count = count

    def posts_count(self):
        return self._posts_count

    def processed_count(self):
        return self._processed_count

    def is_processing_finished(self):
        return self.is_finished
