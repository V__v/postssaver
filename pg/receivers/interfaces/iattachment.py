class IAttachment:
    def __init__(self):
        raise NotImplementedError()

    def id(self):
        raise NotImplementedError()

    def url(self):
        raise NotImplementedError()
