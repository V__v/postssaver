class IReceiverImpl:
    def __init__(self):
        raise NotImplementedError()

    def has_more(self):
        raise NotImplementedError()

    def get_posts(self):
        raise NotImplementedError()

    def get_posts_count(self):
        raise NotImplementedError()
