class IReceiver:
    def __init__(self):
        raise NotImplementedError()

    def has_more(self):
        raise NotImplementedError()

    def get_posts(self):
        raise NotImplementedError()

    def __iter__(self):
        raise NotImplementedError()
