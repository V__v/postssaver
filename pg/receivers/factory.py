from pg.receivers.rtwitter import rtwitter


def tweets_receiver(nick_name, observer=None):
    return rtwitter.TwitterReceiver(nick_name, observer)


def vk_receiver():
    raise NotImplementedError()
