from pg.receivers.interfaces.ipost import IPost
from pg.receivers.rtwitter.attachment import TwitterAttachment


class TwitterPost(IPost):
    def __init__(self, post):
        self.post = post

    def id(self):
        try:
            return self.post['id_str']
        except KeyError as e:
            print("Wrong key " + str(e))
            return ""

    def text(self):
        try:
            return self.post['text']
        except KeyError as e:
            print("Wrong key " + str(e))
            return ""

    def date(self):
        try:
            return self.post['created_at']
        except KeyError as e:
            print("Wrong key " + str(e))
            return ""

    def likes(self):
        try:
            return self.post['favorite_count']
        except KeyError as e:
            print("Wrong key " + str(e))
            return ""

    def attachments(self):
        if 'extended_entities' in self.post:
            return [TwitterAttachment(media) for media in self.post['extended_entities']['media']]
        else:
            return []

    def raw_data(self):
        return self.post
