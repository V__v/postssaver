from pg.receivers.rtwitter.post import TwitterPost, TwitterAttachment
import unittest
import json


class TwitterPostTests(unittest.TestCase):
    def setUp(self):
        self.test_file = "test_data/video_twit.json"
        self.test_data = json.loads(open(self.test_file, 'r').read())
        self.tested_post = TwitterPost(self.test_data)
        self.tested_post_bad = TwitterPost(json.loads("{}"))

    def test_init(self):
        self.assertEqual(self.tested_post.post, self.test_data)

    def test_id(self):
        self.assertEqual(self.tested_post.id(), self.test_data['id_str'])

    def test_text(self):
        self.assertEqual(self.tested_post.text(), self.test_data['text'])

    def test_date(self):
        self.assertEqual(self.tested_post.date(), self.test_data['created_at'])

    def test_likes(self):
        self.assertEqual(self.tested_post.likes(), self.test_data['favorite_count'])

    def test_id_keyerror(self):
        self.assertEqual(self.tested_post_bad.id(), "")

    def test_text_keyerror(self):
        self.assertEqual(self.tested_post_bad.text(), "")

    def test_date_keyerror(self):
        self.assertEqual(self.tested_post_bad.date(), "")

    def test_likes_keyerror(self):
        self.assertEqual(self.tested_post_bad.likes(), "")

    def test_attachments(self):
        attachment = [TwitterAttachment(media) for media in self.test_data['extended_entities']['media']][0]
        tested_attachment = self.tested_post.attachments()[0]
        self.assertEqual(tested_attachment.id(), attachment.id())
        self.assertEqual(tested_attachment.url(), attachment.url())

    def test_attachments_empty(self):
        self.tested_post.post.pop('extended_entities')
        self.assertEqual(self.tested_post.attachments(), [])
