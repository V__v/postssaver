from pg.receivers.rtwitter.attachment import TwitterAttachment
import unittest
import json


class TwitterAttachmentTests(unittest.TestCase):
    def setUp(self):
        self.test_file = "test_data/video_twit.json"
        self.test_data = json.loads(open(self.test_file, 'r').read())['extended_entities']['media'][0]
        self.tested_attachments = TwitterAttachment(self.test_data)
        self.tested_attachments_bad = TwitterAttachment(json.loads("{}"))

    def test_init(self):
        self.assertEqual(self.tested_attachments.obj, self.test_data)

    def test_id(self):
        self.assertEqual(self.tested_attachments.id(), self.test_data['id_str'])

    def test_url_is_video_info(self):
        self.assertEqual(self.tested_attachments.url(), self.test_data['video_info']['variants'][0]['url'])

    def test_url_isnot_video_info(self):
        self.tested_attachments.obj.pop('video_info')
        self.assertEqual(self.tested_attachments.url(), self.test_data['media_url'])

    def test_id_keyerror(self):
        self.assertEqual(self.tested_attachments_bad.id(), "")

    def test_url_keyerror(self):
        self.assertEqual(self.tested_attachments_bad.url(), "")
