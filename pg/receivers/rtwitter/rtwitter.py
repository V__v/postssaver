from pg.receivers.interfaces.ireceiver import IReceiver
from pg.receivers.rtwitter.rtwitter_impl import TwitterReceiverImpl
import urllib


class TwitterReceiver(IReceiver):
    def __init__(self, nick_name, observer=None):
        self.nick = nick_name
        self.impl = TwitterReceiverImpl(self.nick)
        self.posts = None
        self.index = -1
        self.is_iterated = False
        self.observer = observer
        self.observer.set_posts_count(self.impl.get_posts_count())

    def has_more(self):
        return self.impl.has_more()

    def get_posts(self):
        try:
            self.posts = self.impl.get_posts()
            if self.observer:
                self.observer.inc(len(self.posts))
            return self.posts
        except urllib.error.URLError as e:
            print('Network error.')
            print('Error message: ' + str(e))
            self.posts = None
        return None

    def __iter__(self):
        return self

    def __next__(self):
        if self.is_iterated:
            raise StopIteration
        self.index += 1
        if self.posts is None or (self.index == len(self.posts) and self.has_more()):
            self.get_posts()
            self.index = 0
            if self.posts is None:
                raise StopIteration
        if self.index == len(self.posts) and not self.has_more():
            self.is_iterated = True
            raise StopIteration
        if self.observer:
            self.observer.inc()
        return self.posts[self.index]
