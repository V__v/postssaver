from pg.receivers.interfaces.ireceiver_impl import IReceiverImpl
from pg.receivers.rtwitter.post import TwitterPost
import twitter

CONSUMER_KEY = "O8cJSxaNG8c2TzhT15dbS1Zqr"
CONSUMER_SECRET = "bvIbLW1Rxii4JRtDaY8597iT3DouhsJLTpCM6MVpce11rFnU38"
OAUTH_TOKEN = "846696213332332545-QCmikNd5PUUpUnCKmPIbZB3UnjtKcRm"
OAUTH_SECRET = "tMnukpVVIXN1Ccq5A1kV0B8jpgrjUTXx9IxBlFnlcJydD"
MAX_POSTS_COUNT = 3200


class TwitterReceiverImpl(IReceiverImpl):
    def __init__(self, nick_name):
        self.nick = nick_name
        oauth = twitter.OAuth(OAUTH_TOKEN, OAUTH_SECRET, CONSUMER_KEY, CONSUMER_SECRET)
        self.handler = twitter.Twitter(auth=oauth)
        self.time_line = None
        self.last_id = 0

    def has_more(self):
        if self.time_line is None:
            return True
        else:
            return True if self.time_line and self.last_id != self.time_line[-1]['id'] else False

    def get_posts(self):
        if self.time_line is None:
            self.time_line = self.handler.statuses.user_timeline(screen_name=self.nick, exclude_replies=True, count=200)
        else:
            self.last_id = self.time_line[-1]['id']
            self.time_line = self.handler.statuses.user_timeline(screen_name=self.nick, exclude_replies=True,
                                                                 max_id=self.last_id, count=200)
        return [TwitterPost(p) for p in self.time_line]

    def get_posts_count(self):
        tweet = self.handler.statuses.user_timeline(screen_name=self.nick, exclude_replies=True, count=1)
        posts_count = int(tweet[0]['user']['statuses_count'])
        return posts_count if posts_count <= MAX_POSTS_COUNT else MAX_POSTS_COUNT
