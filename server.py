import sys
sys.path.append('pg')
import pg.facade as pgf
import pg.observer as pgo
import threading
import shutil
from flask import Flask, render_template, request, redirect, url_for, send_file


UPLOADS_DIR = '/tmp/uploads'

app = Flask(__name__)
observer = pgo.Observer()


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/user/<socicalnet>', methods=['POST', 'GET'])
def user(socicalnet):
  if request.method == 'POST':
    return redirect(url_for('data',
                            socicalnet=socicalnet,
                            usernick=request.form['usernick']))
  return render_template('user.html', socicalnet=socicalnet)


@app.route('/data/<socicalnet>/<usernick>')
def data(socicalnet, usernick):
  return render_template('data.html',
                         socicalnet=socicalnet,
                         usernick=usernick)


@app.route('/download/<socicalnet>/<usernick>/<datatype>')
def download(socicalnet, usernick, datatype):
    path = upload_path(socicalnet, usernick, datatype)
    thread = threading.Thread(target=pgf.save_tweets, args=(usernick, True, path, observer))
    thread.daemon = True
    thread.start()
    log(sys._getframe().f_code.co_name, 'observer.posts_count(): ' + str(observer.posts_count()))
    return render_template('download.html',
                            socicalnet=socicalnet,
                            usernick=usernick,
                            datatype=datatype)


@app.route('/download/progress')
def progress():
    log(sys._getframe().f_code.co_name, 'observer.posts_count(): ' + str(observer.posts_count()))
    log(sys._getframe().f_code.co_name, 'observer.processed_count(): ' + str(observer.processed_count()))
    if observer.posts_count() == 0 or observer.is_processing_finished():
        log(sys._getframe().f_code.co_name, 'in if!!!')
        return '100'
    return str(round(observer.processed_count() / observer.posts_count() * 100))


@app.route('/uploads/<socicalnet>/<usernick>/<datatype>')
def download_file(socicalnet, usernick, datatype):
    path = upload_path(socicalnet, usernick, datatype)
    arch_path = zip_path(socicalnet, usernick, datatype)
    shutil.make_archive(arch_path, 'zip', path)
    return send_file(arch_path + '.zip', as_attachment=True)


def upload_path(socicalnet, usernick, datatype):
    return UPLOADS_DIR + '/' + socicalnet + '/' + usernick + '/' + datatype


def zip_path(socicalnet, usernick, datatype):
    return UPLOADS_DIR + '/' + socicalnet + '_' + usernick + '_' + datatype


def run():
    app.run(debug=True, host='0.0.0.0')


def log(func, text):
    with open("log.txt", "a") as f:
        f.write('{:<25} \"{}\"'.format(func, text))


if __name__ == '__main__':
    run()
