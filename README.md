# Posts Saver #
http://postssaver.herokuapp.com/

Demo app for [PostsGettet lib](https://bitbucket.org/V__v/postsgetter)

**Python 3.6 is required!**

For installing necessary libs run from command prompt
```sh
pip install -r requirements.txt
```

For run server:
```sh
python server.py
```